<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Foundation | Welcome</title>
	<link rel="stylesheet" href="css/foundation.css" />
	
	<script type="text/javascript">

		function disableForm(name){
		//console.log(name);
		//console.log("inside javascript");
		document.getElementById(name).disabled=true;
	}	

</script>
<script src="js/vendor/modernizr.js"></script>

</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1><a href="index.php">IITB CSE</a></h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section -->
			<ul class="right">
				<li class="active"><a href="#" onclick="staffForm()">Staff</a></li>
				<li class="active"><a href="#" onclick="studentForm()">Student</a></li>
			</ul>

		</section>
	</nav>


	<br>
	<center>
	<h4>User Details </h4>	
	<br>
	<table >
	<?php
	if(isset($_POST['studName'])){
		echo "<tr><td>Student Name : </td><td>".$_POST['studName']."</td></tr>";
		echo "<tr><td>Student ID : </td><td>".$_POST['studID']."</td></tr>";
		echo "<tr><td>Expiration Date : </td><td>".$_POST['expDate']."</td></tr>";
		echo "<tr><td>Guide ID : </td><td>".$_POST['guideID']."</td></tr>";
		echo "<tr><td>Address : </td><td>".$_POST['studAddress']."</td></tr>";
		echo "<tr><td>Group : </td><td>".$_POST['group']."</td></tr>";
	}else if(isset($_POST['staffName'])){
		echo "<tr><td>Staff Name : </td><td>".$_POST['staffName']."</td></tr>";
		echo "<tr><td>Staff ID : </td><td>".$_POST['staffID']."</td></tr>";
		echo "<tr><td>Expiration Date : </td><td>".$_POST['expDate']."</td></tr>";
		echo "<tr><td>Guide ID : </td><td>".$_POST['guideID']."</td></tr>";
		echo "<tr><td>Address : </td><td>".$_POST['staffAddress']."</td></tr>";
		echo "<tr><td>Group : </td><td>".$_POST['group']."</td></tr>";
	}else{
		header('Location: aprove.php');
	}
		//create ldap account
		//deactivate the button
		//echo "here post data will be printed" . $_POST["btn"] . $_POST["group"];
	//echo "<script>disableForm(".'"' . $_POST["btn"] .'"'. ")</script>"
	?>
	</table></center>
	
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
	$(document).foundation();
</script>
</body>
</html>
