<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Foundation | Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
	<script src="js/vendor/modernizr.js"></script>

    <script type="text/javascript">
    
		function disableForm(name){
        //console.log(name);
		//console.log("inside javascript");
			document.getElementById(name).disabled=true; } 
	</script>

  </head>
  <body>
    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.html">IITB CSE</a></h1>
        </li>
      </ul>
    </nav>
    <div class="large-12 medium-12 columns" id="formContainer">
	<?php
	if($_POST["uName"]!="admin" || $_POST["passwd"] != "swlabsysad"){
		//dont allow inside
		exit("wrong username or password");
	}
		$myfile = fopen("student.csv",'r') or die("Unable to open file!");
		$i=1;
		while( !feof($myfile)){
			$entry = fgetcsv($myfile,":");
			$name = "form" . $i;
			if($entry[0] != ""){
				echo '<div class=" row">' .
					  '<form name=' . $name . ' action="action.php">' .
					  '<div class="large-2 medium-2 columns ">' . $entry[0] . "</div>" .
				      '<div class="large-2 medium-2 columns">' . $entry[1] . "</div>" .
				      '<div class="large-2 medium-2 columns">' . $entry[2] . "</div>" .
				      '<div class="large-2 medium-2 columns">' . $entry[3] . "</div>" .
				      '<div class="large-2 medium-2 columns">' . $entry[4] . "</div>" .
				      '<div class="large-1 medium-1 columns">' . $entry[5] . "</div>" .
					  '<input type="submit" value=' . '"' . $name . '"' . ' name="btn">' .
				      "</form>" .
				     "<br/> </div>";
			}
			$i = $i+1;
		}
		fclose($myfile);
		$myfile = fopen("staff.csv",'r') or die("Unable to open file!");
		while( !feof($myfile)){
			$entry = fgetcsv($myfile,":");
			$name = "form" . $i;
			if($entry[0] != ""){
				echo '<div class="row">' .
					  '<form name='.  $name . ' action="action.php" method="post">' .
					  '<div class="large-2 medium-2 columns" name="div0">' . $entry[0] . "</div>" .
					  '<div class="large-2 medium-2 columns" name="div1">' . $entry[1] . "</div>" .
					  '<div class="large-2 medium-2 columns" name="div2">' . $entry[2] . "</div>" .
					  '<div class="large-2 medium-2 columns" name="div3">' . $entry[3] . "</div>" .
					  '<div class="large-2 medium-2 columns" name="div4">' . $entry[4] . "</div>" .
					  '<div class="large-1 medium-1 columns" name="div5">' . $entry[5] . "</div>" .
				      '<input type="submit" value=' . '"' . $name . '"' . ' name="btn">' .
				      "</form>" .
				     "<br/> </div>";
			}
			$i = $i+1;
		}
		fclose($myfile);
	?>
	
	<?php
		//create ldap account
		//deactivate the button
		//echo "here post data will be printed"
		echo "<script>disableForm(".$_POST["btn"] . ")</script>";
	?>

	</div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
