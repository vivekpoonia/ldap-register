<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Foundation | Welcome</title>
	<link rel="stylesheet" href="css/foundation.css" />
	<script src="js/vendor/modernizr.js"></script>

</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1><a href="index.php">IITB CSE</a></h1>
			</li>
		</ul>
		<section class="top-bar-section">
			<!-- Right Nav Section -->
			<ul class="right">
				<li class="active">
					<?php
					session_start();
					if(isset($_SESSION['username'])){
						echo "<a href=logout.php>Logout</a>";
					}
					?>
				</li>
			</ul>
		</section>
	</nav>
	<div class="large-12 medium-12 columns" id="formContainer">
		<?php
		

		if(isset($_SESSION['username']))
		{
			echo "<div align=right>You are logged in as " . $_SESSION['username'] . "</div>";
		} 
		else{
			echo "Session is invalid.";
			header("Location: login.php");
		}	




		echo "<center><h3>List of IDs to be approved </h3>";
		$myfile = fopen("student.csv",'r') or die("Unable to open file!");
		$i=1;
		while( !feof($myfile)){
			$entry = fgetcsv($myfile,0,":");
			$name = "Form" . $i;
			if($entry[0] != ""){
				echo '<div class="columns">' .
				'<form name=' . $name . ' action="action.php" method="POST" name="Approve">' . "<table>".
				'<tr><td>' ."<b>Student Name : </b>"."</td><td>"."<input name='studName' value=$entry[0] readonly></input>". "</td></tr>" .
				'<tr><td>' ."<b>Student ID : </b>"."</td><td>". "<input name='studID' value=$entry[1] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Expiration Date : </b>"."</td><td>". "<input name='expDate' value=$entry[2] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Guide ID : </b>"."</td><td>". "<input name='guideID' value=$entry[3] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Address : </b>"."</td><td>". "<input name='studAddress' value=$entry[4] readonly></input>" . "</td></tr>" .
				'<tr><td>' . "<b>Password : </b>"."</td><td>". "********"/*$entry[5] */. "</td></tr>" .
				'<tr><td><b>Group : </b></td><td><input type="text" name="group"/></td></tr>' .
				'<tr><td colspan=2><center><input type="submit" value=' . '"Approve ' . $name . '"' . ' name="btn"/></center></td></tr>' . "</table>".
				"</form>" .
				"<br/> </div>";
			}
			$i = $i+1;
		}
		fclose($myfile);
		$myfile = fopen("staff.csv",'r') or die("Unable to open file!");
		while( !feof($myfile)){
			$entry = fgetcsv($myfile,0,":");
			$name = "Form" . $i;
			if($entry[0] != ""){
				echo '<div class="columns">' .
				'<form name='.  $name . ' action="action.php" method="post">' ."<table>".
				'<tr><td>' ."<b>Staff Name : </b>". "</td><td>"."<input name='staffName' value=$entry[0] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Staff ID : </b>". "</td><td>"."<input name='staffID' value=$entry[1] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Expiration Date : </b>". "</td><td>"."<input name='expDate' value=$entry[2] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Guide ID : </b>"."</td><td>". "<input name='guideID' value=$entry[3] readonly></input>" . "</td></tr>" .
				'<tr><td>' ."<b>Address : </b>". "</td><td>"."<input name='staffAddress' value=$entry[4] readonly></input>" . "</td></tr>" .
				'<tr><td>' . "<b>Password : </b>". "</td><td>"."********"/*$entry[5] */. "</td></tr>" .
				'<tr><td><b>Group : </b></td><td>	<input type="text" name="group"></td></tr>' . 
				'<tr><td colspan = 2><center><input type="submit" value=' . '"Approve ' . $name . '"' . ' name="btn"/></center></td></tr>' . "</table>".
				"</form>" .
				"<br/> </div>";
			}
			$i = $i+1;
		}
		fclose($myfile);
		?>

	</div>

	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>
