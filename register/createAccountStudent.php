<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script type="text/javascript" src="mysite.js"></script>
  </head>
  <body>
    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">IITB CSE</a></h1>
        </li>
      </ul>
    </nav>
    <div class="large-6 large-centered medium-6 columns" id="formContainer">
      
    <?php 
     
     if(!isset($_POST["studName"]) || $_POST["studName"] == ''){
     		die("No data submitted.");		
     }
      
     /*echo "Student Name : " . $_POST["studName"] . "<br>";
     echo "Student ID : " . $_POST["studID"] . "<br>";
     echo "Student Expiry Date : " . $_POST["expDate"] . "<br>";*/

     /*echo $_POST["studAddr"] . " " . $_POST["guideID"];
     if (!file_exists("student.csv")) {
  	    echo 'File not found';
      }
      $handle = fopen("student.csv",'a+') or die("Unable to open file!");
      fputcsv($handle, $fields,':' );
			fclose($handle);*/
			
    // multiple recipients 
    $message = '<html>  <head>  <title>Staff LDAP creation</title>  </head>';
    $message .=  '<body> <p>Inforamtion</p>';
    $message .= '<table>  <tr> Name: '.$_POST["studName"].'</tr> <tr> ID: '.$_POST["studID"];
    $message .= '</tr> <tr> Exp Date: '.$_POST["expDate"].'</tr> <tr> Guide ID:'.$_POST["guideID"];
    $message .= '</tr> <tr> Address: '.$_POST["studAddr"].'</tr> <tr> Password: '.$_POST["passwd"].'</tr> </table>';
    $message .= ' </body>  </html> ';
    // multiple recipients 
    require '/usr/share/php5/PHPMailer-master/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp-auth.iitb.ac.in';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'username';                 // SMTP username
    $mail->Password = 'password';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->From = 'mail';
    $mail->FromName = 'Vivek Poonia';
    // Add a recipient
    $mail->addAddress('address-reciepient');                 // Name is optional

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Student LDAP ID creation.'; 
    $mail->Body    = $message;
    if(!$mail->send()) {
        echo "Your request could not be submitted to properly. Please contact sysads via mail.";
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo "Your request has been submitted to us. We will soon verify the Details and create your LDAP-ID.";
    }
            ?>
      
    </div>    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
