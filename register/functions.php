<?php
$hosts = array(1=>"1",2=>"2",3=>"3",4=>"4");
$suffix = ".ldap.cse.iitb.ac.in";

function ldap_auth_sysads($user,$pass){
	#	echo "user and passwd ".$user." ".$pass."<br>";
	#$ldaphost="1.ldap.cse.iitb.ac.in";
	global $hosts, $suffix;
	$basedn="ou=People,dc=cse,dc=iitb,dc=ac,dc=in";

	if($user == "psr" || $user == "psgaikwad" || $user == "ravism" || $user == "indrajit" || $user == "priyanka" || $user == "ranjithk" || $user == "smahajan" || $user == "murukesh" || $user == "anubhav" || $user == "jigarab" || $user == "zxparesh" || $user == "kurien" || $user == "msankith" || $user == "asifali" || $user == "shaurya"){
		foreach($hosts as $start){
			// $ldaphost = "1.ldap.cse.iitb.ac.in";#$start.$suffix;
			$ldaphost = $start.$suffix;
			$ds=ldap_connect($ldaphost);
			if($ds == null)
				continue;	#check next server
			//   added ... psr
			@ldap_set_option ($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
			$ldapbindres=ldap_bind($ds);
			$ldapsr=ldap_search($ds,$basedn, "uid=$user");  
			$ldapinfo = ldap_get_entries($ds, $ldapsr);

			/*if($ldapinfo[0]["dn"]==""){
				return 0; // Unauthorized;
			}*/

			#echo "dn is: ". $ldapinfo[0]["dn"] ."<br>";
			$ds1=ldap_connect($ldaphost);  // must be a valid LDAP server!
			//   added ... psr
			@ldap_set_option ($ds1, LDAP_OPT_PROTOCOL_VERSION, 3);
			if (! ldap_start_tls ($ds1)){
				echo "could not start TLS.";
				return 0;
			}

			@$ldapres=ldap_bind($ds1,$ldapinfo[0]["dn"],$pass);      // this is an "anonymous" bind, typically
			if(!$ldapres){
				#echo "Unauthorized - please enter a valid user/password<br>";
				return 0; #un#authorized
			}else{
				#echo "Authorized<br>";
				return 1; ##authorized
			}
			#echo "Closing connection";
			ldap_close($ds1);
			ldap_close($ds);
		}
	}
	else 
		return 0; //Not a sysad
}

function ldap_auth_rs($user,$pass){
	#	echo "user and passwd ".$user." ".$pass."<br>";
	global $hosts, $suffix;
	#$ldaphost="1.ldap.cse.iitb.ac.in";
	$basedn="ou=RS,ou=Students,ou=People,dc=cse,dc=iitb,dc=ac,dc=in";
	foreach($hosts as $start){
		$ldaphost = $start.$suffix;
		$ds=ldap_connect($ldaphost);
		if($ds == null)
			continue;
		//   added ... psr
		@ldap_set_option ($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		$ldapbindres=ldap_bind($ds);
		$ldapsr=ldap_search($ds,$basedn, "uid=$user");  
		$ldapinfo = ldap_get_entries($ds, $ldapsr);
		if(($ldapinfo['count']==0)||($ldapinfo[0]["dn"]==""))
		{
			return 0; // Unauthorized;
		}
		#echo "dn is: ". $ldapinfo[0]["dn"] ."<br>";
		$ds1=ldap_connect($ldaphost);  // must be a valid LDAP server!
		//   added ... psr
		@ldap_set_option ($ds1, LDAP_OPT_PROTOCOL_VERSION, 3);
		if (! ldap_start_tls ($ds1)){
			echo "could not start TLS.";
			return 0;
		}
		@$ldapres=ldap_bind($ds1,$ldapinfo[0]["dn"],$pass);      // this is an "anonymous" bind, typically
		if(!$ldapres){
			#echo "Unauthorized - please enter a valid user/password<br>";
			return 0; #un#authorized
		}else{
			#echo "Authorized<br>";
			return 1; ##authorized
		}
		#	echo "Closing connection";
		ldap_close($ds1);
		ldap_close($ds);
	}
}


function ldap_auth_fac($user,$pass){
	#	echo "user and passwd ".$user." ".$pass."<br>";
	#$ldaphost="1.ldap.cse.iitb.ac.in";
	global $hosts, $suffix;
	$basedn="ou=Faculty,ou=People,dc=cse,dc=iitb,dc=ac,dc=in";
	foreach($hosts as $start){
		$ldaphost = $start.$suffix;
		$ds=ldap_connect($ldaphost);
		if($ds == null)
			continue;
		//   added ... psr
		@ldap_set_option ($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		$ldapbindres=ldap_bind($ds);
		$ldapsr=ldap_search($ds,$basedn, "uid=$user");  
		$ldapinfo = ldap_get_entries($ds, $ldapsr);
		if(($ldapinfo['count']==0)||($ldapinfo[0]["dn"]==""))
		{
			return 0; // Unauthorized;
		}
		#echo "dn is: ". $ldapinfo[0]["dn"] ."<br>";
		$ds1=ldap_connect($ldaphost);  // must be a valid LDAP server!
		//   added ... psr
		@ldap_set_option ($ds1, LDAP_OPT_PROTOCOL_VERSION, 3);
		if (! ldap_start_tls ($ds1)){
			echo "could not start TLS.";
			return 0;
		}
		@$ldapres=ldap_bind($ds1,$ldapinfo[0]["dn"],$pass);      // this is an "anonymous" bind, typically
		if(!$ldapres){
			#echo "Unauthorized - please enter a valid user/password<br>";
			return 0; #un#authorized
		}else{
			#echo "Authorized<br>";
			return 1; ##authorized
		}
		#	echo "Closing connection";
		ldap_close($ds1);
		ldap_close($ds);
	}
}

?>
