<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script type="text/javascript" src="mysite.js"></script>
  </head>
  <body>
    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">IITB CSE</a></h1>
        </li>
      </ul>

      <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
          <li class="active">
          <?php
          session_start();
          if(isset($_SESSION['username'])){
            echo "<a href=logout.php>Logout</a>";
          }else{
            echo "<a href=login.php>Login</a>";
          }
          ?>
          </li>
          <li class="inactive" style="background:#3CAD86;">
          <?php
          session_start();
          if(isset($_SESSION['username'])){
            echo "<a href=aprove.php>Approve Users</a>";
          }
          ?>
          </li>
        </ul>
      </section>
    </nav>
    <div class="large-6 large-centered medium-6 columns" id="formContainer">
      <form id="dataForm">
      <center>
		  <p><h3>Welcome to LDAP ID creation interface.</h3> </p>
		  <p><h3>Choose whether you are staff or student and continue for LDAP ID creation</h3></p>
        <ul class="button-group even-2">
          <li><a class="button" onclick="staffForm()">Staff</a></li>
          <li><a class="button" onclick="studentForm()">Student</a></li>
        </ul>
      <center>
      </form>
    </div>    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
