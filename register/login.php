<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Foundation | Welcome</title>
	<link rel="stylesheet" href="css/foundation.css" />
	<script src="js/vendor/modernizr.js"></script>
	<script src="mysite.js"></script>

</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1><a href="index.php">IITB CSE</a></h1>
			</li>
		</ul>
	</nav>
	<div class="large-12 medium-12 columns" id="formContainer">
		<?php
		session_start();

		function show_login_form()
		{

			$PHP_SELF = $_SERVER['PHP_SELF'];
			#echo"<body bgcolor=#ebebeb align=center style='font-family:helvetica;'>";
			echo "<form  action=\"$PHP_SELF\" method=\"POST\" name \"loginform\">\n"; 
			?>
			<center><h2>Login</h2></center><hr>
			<input type="hidden" name="action" value="login">
			<table align="center"> 
				<tr>
					<td bgcolor='#DCDCDC' colspan='2' width='500' align='center'><b>Approver LDAP Authentication</b></td>
				</tr>
				<tr>
					<td align='right' width='35%'>Username:</td>
					<td align='left' width='50%'><input type='text' name='username' value=''></td>
				</tr>
				<tr>
					<td align='right' width='35%'>Password:</td>
					<td align='left' width='50%'><input type='password' name='password'></td>
				</tr>
				<tr>
					<td align='center' colspan='2'><input type='submit' name='Login' value='Login'></td>
				</tr>
				<tr>
					<td bgcolor='#DCDCDC' colspan='2' align='center'>&nbsp;</td>
				</tr>
			</table>
		</form>
		
		<?php
		}
		//LDAP Authentication

		require_once('functions.php');
		
		//Start session
		if(isset($_SESSION['valid']))
		{
			echo "<h3> You are already logged in as " . $_SESSION['username'] . "</h3>\n";
			echo "<p>If you are not this user,please <a href=\"logout.php\">logout</a> and login with your account. </p> \n";
		} elseif($_POST['action'] == "login") { 
			$username = $_POST['username'];
			$password = $_POST['password'];

			if(ldap_auth_sysads($username,$password) == true) {
				$_SESSION['valid'] = true;
				$_SESSION['username'] = $username;
				echo "Login Success";
				header("Location: aprove.php\n\n");
			} else {			
				session_unset();
				session_destroy();
				echo "<font color=red>Login failed. Please retry.</font>";
				show_login_form();
			}	

		} else {
			show_login_form();
		}
		?>

	</div>

	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>
