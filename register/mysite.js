
//function for verification of password on client end.
function verify() {

    var passwd = document.getElementById("passwd");
    var verpasswd = document.getElementById("verPasswd");
    if(passwd.value !== verpasswd.value){
      console.log(passwd.value);
      console.log(verpasswd.value);
      passwd.value="";
      verpasswd.value="";
      document.alert("You entered different paswords. Please re enter.");
    }

}


//creates input along with a label for it
//we will use it for form creation
function createInput(type, id,labelText,parentForm ){

    var inputElement = document.createElement("input");
    inputElement.type=type;
    inputElement.id=id;
    inputElement.name=id;
    inputElement.class="large-4 medium-4 columns";
    inputElement.required=true;
    var label = document.createElement("Label");
    label.for = inputElement;
    label.innerHTML=labelText.concat("(*).");

    parentForm.appendChild(inputElement);
    parentForm.insertBefore(label, inputElement);
    parentForm.insertBefore(document.createElement("BR"), label);

}


//clears out div on every time we call it.
//we will use it to clear the formCOntainer div when creating registration
//form for student and staff
function refresh(type){

  var formContainer = document.getElementById("formContainer");
  formContainer.removeChild(document.getElementById("dataForm"));
  var form = document.createElement("form");
  form.id="dataForm";

  if(type=="student"){
    form.action="createAccountStudent.php";  
  }else if(type=="staff"){
    form.action="createAccountStaff.php";
  }else{
	form.action="aprove.php"
  }
  form.method="post";
  form.onSubmit="verify()";
  formContainer.appendChild(form);

}


function staffForm(){


 // var form = document.getElementById("staff");
  //if(form.checked){

        refresh("staff");
        appendForm=document.getElementById("dataForm");
        createInput("text","staffName","Your Name", appendForm);
        createInput("text","staffID", "Employee Code",appendForm);
        createInput("date","expDate", "Expiry date on ID",appendForm);
        createInput("text","guideID", "CSE-ID of incharge",appendForm);
        createInput("text","staffAddr", "Your campus address",appendForm);
        createInput("password","passwd","Enter Password",appendForm);
        createInput("password","verPasswd","Verify Password",appendForm);
        var btnSubmit = document.createElement("input");
        btnSubmit.type="submit";
        btnSubmit.value="Submit";
        btnSubmit.onsubmit="verify()";
        appendForm.appendChild(btnSubmit);

  //}


}

function studentForm(){

  //var form = document.getElementById("student");
  //if(form.checked)
        refresh("student");
        appendForm=document.getElementById("dataForm");
        createInput("text","studName","Your Name", appendForm);
        createInput("text","studID", "Roll no",appendForm);
        createInput("date","expDate", "Expiry date ID",appendForm);
        createInput("text","guideID", "CSE ID of your Guide",appendForm);
        createInput("text","studAddr", "Your campus address",appendForm);
        createInput("password","passwd","Enter Password",appendForm);
        createInput("password","verPasswd","Re-enter Password",appendForm);

        var btnSubmit = document.createElement("input");
        btnSubmit.type="submit";
        btnSubmit.value="Submit";
        btnSubmit.onsubmit="return verify()";
        appendForm.appendChild(btnSubmit);
  //}

}

function loginForm(){

  //var form = document.getElementById("student");
  //if(form.checked)
        refresh("aprove");
        appendForm=document.getElementById("dataForm");
        createInput("text","uName","Username", appendForm);
        createInput("password","passwd", "Password",appendForm);
        var btnSubmit = document.createElement("input");
        btnSubmit.type="submit";
        btnSubmit.value="Submit";
        appendForm.appendChild(btnSubmit);
  //}
}
